package ru.t1.dkononov.tm.integration.rest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.t1.dkononov.tm.entity.dto.Result;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class TasksEndpointTest {

    private static String sessionId;

    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    private static final String TASK_URL = "http://localhost:8080/api/task/";

    final TaskDto taskDto1 = new TaskDto("Test 1");

    final TaskDto taskDto2 = new TaskDto("Test 2");

    final TaskDto taskDto3 = new TaskDto("Test 3");

    final TaskDto taskDto4 = new TaskDto("Test 4");

    final List<TaskDto> taskDtoList1 = Arrays.asList(taskDto1, taskDto2);

    final List<TaskDto> taskDtoList2 = Arrays.asList(taskDto3, taskDto4);

    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        final RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDto> sendRequest(final String url, final HttpMethod method, final HttpEntity httpEntity) {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDto.class);
    }

    @Before
    public void initTest() {
        final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList1, header));
    }

    @After
    public void clean() {
        final String url = BASE_URL + "removeAll/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @AfterClass
    public static void logout() {
        final RestTemplate restTemplate = new RestTemplate();
        final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        final String url = BASE_URL + "add/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList2, header));
        final String findUrl = TASK_URL + "findById/" + taskDto4.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final String url = BASE_URL + "findAll/";
        Assert.assertEquals(3, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void remove() {
        final String url = BASE_URL + "remove/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(taskDtoList1, header));
        final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

}
