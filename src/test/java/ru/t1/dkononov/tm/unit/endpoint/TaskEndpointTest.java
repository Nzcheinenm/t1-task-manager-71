package ru.t1.dkononov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.dkononov.tm.config.ApplicationConfiguration;
import ru.t1.dkononov.tm.config.WebApplicationConfiguration;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.marker.UnitCategory;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
public class TaskEndpointTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    private MockMvc mockMvc;

    private WebApplicationContext wac;

    private static final String TASK_URL = "http://localhost:8080/api/task/";

    private static final String TASKS_URL = "http://localhost:8080/api/tasks/";

    private final TaskDto task1 = new TaskDto("Test Task 1");

    private final TaskDto task2 = new TaskDto("Test Task 2");

    private final TaskDto task3 = new TaskDto("Test Task 3");

    private final TaskDto task4 = new TaskDto("Test Task 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        add(task1);
        add(task2);
    }

    @After
    @SneakyThrows
    public void afterTest() {
        mockMvc.perform(MockMvcRequestBuilders.delete(TASKS_URL + "removeAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(final TaskDto taskDto) {
        String url = TASK_URL + "add";
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskDto);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void addAll(final List<TaskDto> tasks) {
        String url = TASKS_URL + "add";
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tasks);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private TaskDto findById(final String id) {
        String url = TASK_URL + "findById/" + id;
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TaskDto.class);
    }

    @SneakyThrows
    private List<TaskDto> findAll() {
        String url = TASKS_URL + "findAll";
        final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, TaskDto[].class));
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        add(task3);
        final TaskDto task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task1.getId(), task.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(findById(task1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeTest() {
        mockMvc.perform(MockMvcRequestBuilders.delete(TASKS_URL + "removeAll")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void testSave() {
        add(task4);
        final TaskDto task = findById(task4.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task4.getId(), task.getId());
    }

}
