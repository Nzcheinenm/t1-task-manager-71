package ru.t1.dkononov.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.config.ApplicationConfiguration;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.utils.UserUtil;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskServiceTest {

    @Autowired
    private TaskDtoService taskDtoService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private final TaskDto task1 = new TaskDto("Test Project 1");

    private final TaskDto task2 = new TaskDto("Test Project 2");

    private final TaskDto task3 = new TaskDto("Test Project 3");

    private final TaskDto task4 = new TaskDto("Test Project 4");

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskDtoService.save(task1);
        taskDtoService.save(task2);
    }

    @After
    public void afterTest() {
        taskDtoService.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskDtoService.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, taskDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        taskDtoService.deleteById(task1.getId());
        Assert.assertNull(taskDtoService.findById(task1.getId()));
    }

}
