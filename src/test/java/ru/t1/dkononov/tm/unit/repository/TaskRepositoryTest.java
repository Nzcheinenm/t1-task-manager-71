package ru.t1.dkononov.tm.unit.repository;

import org.apache.commons.configuration.DatabaseConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dkononov.tm.entity.model.Task;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.repository.TaskRepository;
import ru.t1.dkononov.tm.repository.UserRepository;
import ru.t1.dkononov.tm.utils.UserUtil;

import javax.transaction.Transactional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskRepositoryTest {

    private final Task task1 = new Task("Test Task 1");

    private final Task task2 = new Task("Test Task 2");

    private final Task task3 = new Task("Test Task 3");

    private final Task task4 = new Task("Test Task 4");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TaskRepository taskRepository;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUser(userRepository.findById(UserUtil.getUserId()).get());
        task2.setUser(userRepository.findById(UserUtil.getUserId()).get());
        task3.setUser(userRepository.findById(UserUtil.getUserId()).get());
        task4.setUser(userRepository.findById(UserUtil.getUserId()).get());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @Autowired
    public void clean() {
        taskRepository.deleteAll();
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByUserIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task2.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existByUserIdAndIdTest() {
        Assert.assertTrue(taskRepository.existsById(task3.getId()));
    }

}
