package ru.t1.dkononov.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.config.ApplicationConfiguration;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.utils.UserUtil;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectServiceTest {

    @Autowired
    private ProjectDtoService projectDtoService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectDtoService.save(project1);
        projectDtoService.save(project2);
    }

    @After
    public void afterTest() {
        projectDtoService.clear();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectDtoService.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        projectDtoService.deleteById(project1.getId());
        Assert.assertNull(projectDtoService.findById(project1.getId()));
    }

}
