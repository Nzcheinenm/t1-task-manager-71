package ru.t1.dkononov.tm.unit.repository;

import org.apache.commons.configuration.DatabaseConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dkononov.tm.entity.model.Project;
import ru.t1.dkononov.tm.marker.UnitCategory;
import ru.t1.dkononov.tm.repository.ProjectRepository;
import ru.t1.dkononov.tm.repository.UserRepository;
import ru.t1.dkononov.tm.utils.UserUtil;

import javax.transaction.Transactional;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectRepositoryTest {

    private final Project project1 = new Project("Test Project 1");

    private final Project project2 = new Project("Test Project 2");

    private final Project project3 = new Project("Test Project 3");

    private final Project project4 = new Project("Test Project 4");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ProjectRepository projectRepository;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUser(userRepository.findById(UserUtil.getUserId()).get());
        project2.setUser(userRepository.findById(UserUtil.getUserId()).get());
        project3.setUser(userRepository.findById(UserUtil.getUserId()).get());
        project4.setUser(userRepository.findById(UserUtil.getUserId()).get());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @Autowired
    public void clean() {
        projectRepository.deleteAll();
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByUserIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project2.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existByUserIdAndIdTest() {
        Assert.assertTrue(projectRepository.existsById(project3.getId()));
    }

}
