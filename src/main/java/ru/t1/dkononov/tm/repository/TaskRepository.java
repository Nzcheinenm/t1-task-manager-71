package ru.t1.dkononov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.dkononov.tm.entity.model.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, String> {

    Optional<Task> findByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    List<Task> findAllByUserId(String userId);

}
