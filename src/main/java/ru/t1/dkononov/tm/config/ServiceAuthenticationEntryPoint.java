package ru.t1.dkononov.tm.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import ru.t1.dkononov.tm.entity.dto.MessageDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServiceAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        final PrintWriter writer = httpServletResponse.getWriter();
        final ObjectMapper mapper = new ObjectMapper();
        final String value = e.getMessage();
        final MessageDto message = new MessageDto(value);
        writer.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

}
