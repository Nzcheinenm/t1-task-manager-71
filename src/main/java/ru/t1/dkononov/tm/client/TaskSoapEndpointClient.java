package ru.t1.dkononov.tm.client;

import ru.t1.dkononov.tm.api.endpoint.TaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class TaskSoapEndpointClient {

    public static TaskEndpoint getInstance(final String baseURL) throws MalformedURLException {
        final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        final URL url = new URL(wsdl);
        final String lp = "TaskDtoServiceImpl";
        final String ns = "http://endpoint.tm.dkononov.t1.ru/";
        final QName name = new QName(ns, lp);
        final TaskEndpoint result = Service.create(url, name).getPort(TaskEndpoint.class);
        final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
