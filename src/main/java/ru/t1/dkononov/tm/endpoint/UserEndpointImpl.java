package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.dkononov.tm.api.endpoint.UserEndpoint;
import ru.t1.dkononov.tm.api.service.UserService;
import ru.t1.dkononov.tm.entity.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.UserEndpoint")
public class UserEndpointImpl implements UserEndpoint {

    @Autowired
    private UserService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<UserDto> findAll() {
        return service.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDto save(
            @WebParam(name = "user", partName = "user")
            @RequestBody UserDto user
    ) {
        return service.save(user);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public UserDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/exsitsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.exsistsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody UserDto user
    ) {
        service.delete(user);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void deleteAll(
            @WebParam(name = "users", partName = "users")
            @RequestBody List<UserDto> users
    ) {
        service.deleteAll(users);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void clear() {
        service.clear();
    }

}

