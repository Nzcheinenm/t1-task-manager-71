package ru.t1.dkononov.tm.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.dkononov.tm.api.endpoint.AuthEndpoint;
import ru.t1.dkononov.tm.entity.dto.Result;
import ru.t1.dkononov.tm.entity.dto.UserDto;
import ru.t1.dkononov.tm.service.UserDetailsServiceBean;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.AuthEndpoint")
public class AuthEndpointImpl implements AuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceBean service;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    public UserDto profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return service.findByLogin(username);
    }

    @Override
    @WebMethod
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
