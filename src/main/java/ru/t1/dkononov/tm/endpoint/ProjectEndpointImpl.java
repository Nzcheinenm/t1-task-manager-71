package ru.t1.dkononov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.dkononov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.entity.dto.ProjectDto;
import ru.t1.dkononov.tm.utils.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.dkononov.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectDtoService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public List<ProjectDto> findAll() {
        return service.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDto save(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDto project
    ) {
        project.setUserId(UserUtil.getUserId());
        return service.save(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/exsitsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.exsistsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        service.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDto project
    ) {
        project.setUserId(UserUtil.getUserId());
        service.delete(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDto> projects
    ) {
        service.deleteAll(projects);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void clear() {
        service.clear();
    }

}
